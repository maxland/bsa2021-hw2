﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;

namespace CoolParking.CI
{
    public class ConsoleInterface : IDisposable
    {
        readonly string filename = $@"{ Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) }\Transactions.log";

        readonly IParkingService _parkingService;
        readonly ITimerService _withdrawTimer;
        readonly ITimerService _logTimer;
        readonly ILogService _logService;

        public ConsoleInterface()
        {
            _withdrawTimer = new TimerService(Settings.PaymentChargePeriod);
            _logTimer = new TimerService(Settings.LoggingPeriod);
            _logService = new LogService(filename);
            _parkingService = new ParkingService(_withdrawTimer, _logTimer, _logService);

            _withdrawTimer.Start();
            _logTimer.Start();
        }

        public void Dispose()
        {
            _parkingService.Dispose();

            _withdrawTimer.Stop();
            _withdrawTimer.Dispose();

            _logTimer.Stop();
            _logTimer.Dispose();
        }

        public void Execute()
        {
            char key = '\0';
            while (key != 'q')
            {
                DisplayMainMenu();
                key = Console.ReadKey().KeyChar;
                Console.Clear();

                switch (key)
                {
                    case 'q':
                        return ;
                    case '1':
                        DisplayParkingBalance();
                        break;
                    case '2':
                        DisplayCurrentPeriodErnings();
                        break;
                    case '3':
                        DisplayFreePlaces();
                        break;
                    case '4':
                        DisplayCurrentPeriodTransactions();
                        break;
                    case '5':
                        DisplayTransactionsHistory();
                        break;
                    case '6':
                        DisplayAllVehicles();
                        break;
                    case '7':
                        AddVehicleMenu();
                        break;
                    case '8':
                        DisplayRemoveVehicleMenu();                        
                        break;
                    case '9':
                        DisplayTopUpVehocleMenu();                        
                        break;
                }

                Console.WriteLine();
            }
        }

        private void DisplayMainMenu()
        {
            Console.WriteLine("Choose the option: ");
            Console.WriteLine("[1] - Show current Parking balance");
            Console.WriteLine("[2] - Show the amount of earnings for the current period");
            Console.WriteLine("[3] - Show the number of free parking places");
            Console.WriteLine("[4] - Show all Parking Transactions for the current period");
            Console.WriteLine("[5] - Show transaction history");
            Console.WriteLine("[6] - Show all Vehicles in the Parking");
            Console.WriteLine("[7] - Put the Vehicle in the Parking");
            Console.WriteLine("[8] - Pick up the Vehicle from the Parking");
            Console.WriteLine("[9] - Top up Vehicle balance\n");
            Console.WriteLine("[q] - Quit");
        }

        private void DisplayParkingBalance()
        {
            Console.WriteLine($"Parking balance: {_parkingService.GetBalance()}");
        }

        private void DisplayCurrentPeriodErnings()
        {
            var lastParkingTransactions = _parkingService.GetLastParkingTransactions();
            decimal earnings = lastParkingTransactions.Sum(tr => tr.Sum);

            Console.WriteLine($"Earnings for the current period: {earnings}");
        }

        private void DisplayFreePlaces()
        {
            Console.WriteLine($"{_parkingService.GetFreePlaces()}/{_parkingService.GetCapacity()} parking places are available");
        }

        private void DisplayCurrentPeriodTransactions()
        {
            var lastParkingTransactions = _parkingService.GetLastParkingTransactions();

            Console.WriteLine("All transactions for the current period:");
            foreach (TransactionInfo transactionInfo in lastParkingTransactions)
            {
                Console.WriteLine(transactionInfo.ToString());
            }
        }

        private void DisplayTransactionsHistory()
        {
            try
            {
                Console.Write(_parkingService.ReadFromLog());
            }
            catch (InvalidOperationException)
            {
                Console.WriteLine($"File {filename} does not exist");
            }
            catch (Exception e)
            {
                Console.WriteLine($"Exeption: {e.Message}");
            }
            Console.WriteLine();
        }

        private void DisplayAllVehicles()
        {
            var vehicles = _parkingService.GetVehicles();

            Console.WriteLine("Vehicles in the Parking:");
            foreach (Vehicle vehicle in vehicles)
            {
                Console.WriteLine(vehicle.ToString());
            }
        }

        private void AddVehicleMenu()
        {
            string vehicleId;
            InputVehicleIdMenu(out vehicleId);

            VehicleType vehicleType;
            InputVehicleTypeMenu(out vehicleType);
            
            decimal startBalance;
            InputSumMenu(out startBalance);

            try
            {
                Vehicle vehicle = new Vehicle(vehicleId, vehicleType, startBalance);
                _parkingService.AddVehicle(vehicle);
                Console.WriteLine("The vehicle has been successfully put in parking");
            }
            catch (ArgumentException)
            {
                Console.WriteLine("Invalid value entered or vehicle with this ID is already exist");
            }
            catch (InvalidOperationException)
            {
                Console.WriteLine("The Parking is full");
            }
            catch (Exception e)
            {
                Console.WriteLine($"Exeption: {e.Message}");
            }
        }

        private void InputVehicleIdMenu(out string stringForInput)
        {
            Console.WriteLine("Enter the vehicle ID in the format XX-YYYY-XX, where");
            Console.WriteLine("X - English uppercase letter, Y - decimal digit");
            stringForInput = Console.ReadLine();
            Console.Clear();
        }

        private void InputVehicleTypeMenu(out VehicleType varForInput)
        {
            while (true)
            {
                Console.WriteLine("Choose the vehicle type:");
                Console.WriteLine("[1] - Passenger car");
                Console.WriteLine("[2] - Truck");
                Console.WriteLine("[3] - Bus");
                Console.WriteLine("[4] - Motorcycle");

                char type = Console.ReadKey().KeyChar;

                if (type == '1')
                {
                    varForInput = VehicleType.PassengerCar;
                    break;
                }
                else if (type == '2')
                {
                    varForInput = VehicleType.Truck;
                    break;
                }
                else if (type == '3')
                {
                    varForInput = VehicleType.Bus;
                    break;
                }
                else if (type == '4')
                {
                    varForInput = VehicleType.Motorcycle;
                    break;
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Invalid input. Please try again");
                }
            }

            Console.Clear();
        }

        private void InputSumMenu(out decimal numberForInput)
        {
            while (true)
            {
                Console.WriteLine("Enter the sum:");
                bool isInputValid = decimal.TryParse(Console.ReadLine(), out numberForInput);

                if (isInputValid)
                {
                    break;
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Invalid input. Please try again");
                }
            }
            
            Console.Clear();
        }

        private void DisplayRemoveVehicleMenu()
        {
            string vehicleId;
            InputVehicleIdMenu(out vehicleId);

            try
            {
                _parkingService.RemoveVehicle(vehicleId);
                Console.WriteLine("The vehicle has been successfully picked up from the parking");
            }
            catch (ArgumentException)
            {
                Console.WriteLine("The vehicle with this ID does not exist");
            }
            catch (InvalidOperationException)
            {
                Console.WriteLine("The vehicle has a negative balance!");
                Console.WriteLine("Please top up the balance and try again");
            }
            catch (Exception e)
            {
                Console.WriteLine($"Exeption: {e.Message}");
            }
        }

        private void DisplayTopUpVehocleMenu()
        {
            string vehicleId;
            InputVehicleIdMenu(out vehicleId);

            decimal sum;
            InputSumMenu(out sum);

            try
            {
                _parkingService.TopUpVehicle(vehicleId, sum);
                Console.WriteLine("The vehicle balance has been successfully topped up");
            }
            catch (ArgumentException)
            {
                Console.WriteLine("Invalid value entered or the vehicle with this ID does not exist");
            }
            catch (Exception e)
            {
                Console.WriteLine($"Exeption: {e.Message}");
            }
        }
    }
}
