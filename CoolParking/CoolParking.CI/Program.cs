﻿using System;

namespace CoolParking.CI
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleInterface ci = new ConsoleInterface();
            ci.Execute();
            ci.Dispose();
        }
    }
}
