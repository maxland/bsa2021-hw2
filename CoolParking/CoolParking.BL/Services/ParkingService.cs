﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Timers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        Parking _parking;
        ITimerService _withdrawTimer;
        ITimerService _logTimer;
        ILogService _logService;

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _parking = Parking.GetParking();

            withdrawTimer.Elapsed += OnWithdrawEvent;
            _withdrawTimer = withdrawTimer;

            logTimer.Elapsed += OnLogEvent;
            _logTimer = logTimer;

            _logService = logService;
        }

        public void Dispose()
        {
            _parking.Balance = Settings.ParkingStartBallance;
            _parking.Vehicles.Clear();
            _parking.LastTransactions.Clear();
        }

        public decimal GetBalance()
        {
            return _parking.Balance;
        }

        public int GetCapacity()
        {
            return _parking.Capacity;
        }

        public int GetFreePlaces()
        {
            return _parking.FreePlaces;
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            var vehicles = _parking.Vehicles.Values;
            return vehicles.ToList().AsReadOnly();
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (_parking.Vehicles.ContainsKey(vehicle.Id))
            {
                throw new ArgumentException();
            }

            if (_parking.FreePlaces == 0)
            {
                throw new InvalidOperationException();
            }

            _parking.Vehicles.Add(vehicle.Id, vehicle);
        }

        public void RemoveVehicle(string vehicleId)
        {
            Vehicle vehicle;
            bool isVehicleExist = _parking.Vehicles.TryGetValue(vehicleId, out vehicle);

            if (!isVehicleExist)
            {
                throw new ArgumentException();
            }
            
            if (vehicle.Balance < 0)
            {
                throw new InvalidOperationException();
            }
            
            _parking.Vehicles.Remove(vehicleId);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum < 0)
            {
                throw new ArgumentException();
            }

            Vehicle vehicle;
            bool isVehicleExist = _parking.Vehicles.TryGetValue(vehicleId, out vehicle);

            if (!isVehicleExist)
            {
                throw new ArgumentException();
            }

            vehicle.Balance += sum;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return _parking.LastTransactions.ToArray();
        }

        public string ReadFromLog()
        {
            return _logService.Read();
        }

        private void OnWithdrawEvent(object source, ElapsedEventArgs e)
        {
            ReadOnlyCollection<Vehicle> vehicles = GetVehicles();

            foreach (Vehicle v in vehicles)
            {
                decimal tariff = 0;

                switch (v.VehicleType)
                {
                    case VehicleType.PassengerCar:
                        tariff = Settings.PassengerCarTariff;
                        break;
                    case VehicleType.Truck:
                        tariff = Settings.TruckTariff;
                        break;
                    case VehicleType.Bus:
                        tariff = Settings.BusTariff;
                        break;
                    case VehicleType.Motorcycle:
                        tariff = Settings.PassengerCarTariff;
                        break;
                    default:
                        throw new ArgumentException();
                }

                decimal sum;

                if (v.Balance > 0)
                {
                    if (v.Balance < tariff)
                    {
                        sum = v.Balance + (tariff - v.Balance) * Settings.PenaltyRate;
                    }
                    else
                    {
                        sum = tariff;
                    }
                }
                else
                {
                    sum = tariff * Settings.PenaltyRate;
                }

                v.Balance -= sum;
                _parking.Balance += sum;

                TransactionInfo transaction = new TransactionInfo(DateTime.Now, v.Id, sum);
                _parking.LastTransactions.Add(transaction);
            }
        }

        private void OnLogEvent(object sender, ElapsedEventArgs e)
        {
            StringBuilder resultStringBuilder = new StringBuilder();

            foreach (TransactionInfo transactionInfo in _parking.LastTransactions)
            {
                resultStringBuilder.Append(transactionInfo.ToString());
                resultStringBuilder.Append('\n');
            }

            _logService.Write(resultStringBuilder.ToString());

            _parking.LastTransactions.Clear();
        }
    }
}
