﻿using System;
namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public DateTime Time;
        public string VehicleId;
        public decimal Sum;

        public TransactionInfo(DateTime time, string vehicleId, decimal sum)
        {
            Time = time;
            VehicleId = vehicleId;
            Sum = sum;
        }

        public override string ToString()
        {
            return string.Format("{0:HH:mm:ss} {1} {2}", Time, VehicleId, Sum);
        }
    }
}