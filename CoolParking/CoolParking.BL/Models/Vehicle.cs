﻿// TODO: implement class Vehicle.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated UNIQUE identifier.
using System;
using System.Text;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public static readonly string IdRegex = @"^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$";

        public string Id { get; }
        public VehicleType VehicleType { get; }
        public decimal Balance { get; internal set; }

        public Vehicle(string id, VehicleType type, decimal balance)
        {
            if (Regex.IsMatch(id, IdRegex) == false)
            {
                throw new ArgumentException();
            }

            if (balance < 0)
            {
                throw new ArgumentException();
            }

            Id = id;
            VehicleType = type;
            Balance = balance;
        }

        public override string ToString()
        {
            return $"{Id} {VehicleType} {Balance}";
        }

        private static string GenerateRandomRegistrationPlateNumber()
        {
            int firstPartLength = 2;
            int secondPartLength = 4;
            int thirdPartLength = 2;

            string letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string digits = "0123456789";

            string registrationPlateNumber = GenerateRandomStringFromDictionary(firstPartLength, letters) + "-" +
                                             GenerateRandomStringFromDictionary(secondPartLength, digits) + "-" +
                                             GenerateRandomStringFromDictionary(thirdPartLength, letters);

            return registrationPlateNumber;
        }

        private static string GenerateRandomStringFromDictionary(int destStringLenght, string dictionary)
        {
            Random random = new Random();

            StringBuilder resultStringBuilder = new StringBuilder();
            int dictionaryRange = dictionary.Length;

            for (int i = 0; i < destStringLenght; i++)
            {
                resultStringBuilder.Append(dictionary[random.Next(dictionaryRange)]);
            }

            return resultStringBuilder.ToString();
        }
    }
}
