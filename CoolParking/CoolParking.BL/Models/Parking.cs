﻿using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    class Parking
    {
        private static Parking s_parking;

        public decimal Balance { get; set; }
        public int Capacity { get; }
        public int FreePlaces { get { return Capacity - Vehicles.Count; } }
        public Dictionary<string, Vehicle> Vehicles { get; }
        public List<TransactionInfo> LastTransactions { get; }

        private Parking()
        {
            Balance = Settings.ParkingStartBallance;
            Capacity = Settings.ParkingCapacity;
            Vehicles = new Dictionary<string, Vehicle>(Capacity);
            LastTransactions = new List<TransactionInfo>();
        }

        public static Parking GetParking()
        {
            if (s_parking == null)
            {
                s_parking = new Parking();
            }

            return s_parking;
        }
    }
}