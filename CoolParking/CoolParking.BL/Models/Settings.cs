﻿namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public const decimal ParkingStartBallance = 0;

        public const int ParkingCapacity = 10;
        public const int PaymentChargePeriod = 5;
        public const int LoggingPeriod = 60;

        public const decimal PassengerCarTariff = 2M;
        public const decimal TruckTariff = 5M;
        public const decimal BusTariff = 3.5M;
        public const decimal MotorcycleTariff = 1M;

        public const decimal PenaltyRate = 2.5M;
    }
}
